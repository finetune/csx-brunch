'use strict';

var allowedTags = ['div', 'span', 'input', 'h1', 'h2', 'button', 'small', 'footer', 'nav',
	'a', 'canvas', 'caption', 'center', 'div', 'fieldset',
    'form', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'i', 'img', 'label', 'legend',
    'li', 'ol', 'p', 'span', 'strong', 'sub', 'sup', 'table', 'br',
    'tbody', 'td', 'tfoot', 'th', 'thead', 'tr', 'ul', 'style'];

var compile = function(src){
	//Удаляем символ возврата каретки, если он есть, нам достаточно new line
	src = src.replace(/\r/g, '');

	//Начинаем парсить построчно
	src = src.replace(/^( *)(.+)$/gm, function(){

		var indentationSpaces = arguments[1]; //наденные trailing spaces
		var lineContent = arguments[2]; //непосредственно содержимое строки
		var itIsJadeLine = false; //если алгоритм поймет, что это строка с jade, то здесь будет true
		var probablyJadeExpression;
		var symbolNextToJadeExpression;
		var probablyUnquotedTextInJade = '';



		if (lineContent.indexOf('|') === 0) {
			return indentationSpaces + '\''+ lineContent.substr(1) + '\'';
		}



		var jadeExpressionMatch = lineContent.match(/^(.+?)( |,|\(|\:|\=|$)/);
		if (jadeExpressionMatch) {
			probablyJadeExpression = jadeExpressionMatch[1];
			symbolNextToJadeExpression = jadeExpressionMatch[2];

			if (symbolNextToJadeExpression !==':') {
				if (probablyJadeExpression.indexOf('.') === 0) {
					itIsJadeLine = true;
				} else {
					var probablyTagMatch = probablyJadeExpression.match(/^[^\#^\.:\[]+/);
					var probablyTagName = '';
					if (probablyTagMatch) {
						probablyTagName = probablyTagMatch[0];
						if (allowedTags.indexOf(probablyTagName) > -1 || probablyTagName.indexOf('uma-') === 0) {
							itIsJadeLine = true;
						}
					}
				}
			}



		}
		if(itIsJadeLine) {


			if (symbolNextToJadeExpression === '(') {
				lineContent = lineContent.replace('(', ' ');
				var index = lineContent.lastIndexOf(')');
				lineContent  = lineContent.substr(0, index) + ',' + lineContent.substr(index + 1);
				probablyUnquotedTextInJade = lineContent.substr(index + 1);
			} else {
				probablyUnquotedTextInJade = lineContent.substr(probablyJadeExpression.length);
			}

			lineContent = lineContent.replace(probablyJadeExpression, 'd \''+ probablyJadeExpression+ '\',');
			lineContent = lineContent.replace(new RegExp('\\=', 'g'), ':');

		}

		probablyUnquotedTextInJade = probablyUnquotedTextInJade.replace(/^( *)/, '');
		if (probablyUnquotedTextInJade.indexOf('|') === 0) {
			lineContent = lineContent.replace(probablyUnquotedTextInJade, '\''+ probablyUnquotedTextInJade.substr(1) + '\'');
		}
		return indentationSpaces + lineContent;


	});


	var spaces='';
	//src = src.replace(/\r/g, '')

	for (var i = 0; i <= 40; i++) {
		var addSemicolonToIndented = new RegExp('(\\n' + spaces + 'd )(.+)((\\n| )*)(\\n' + spaces + '\\s+.)', 'g');

		src = src.replace(addSemicolonToIndented, function(){
			return arguments[1] + arguments[2].replace(/,? *$/m, ',') + arguments[5];//;
		});

		var removeSemicolonFromUnindented = new RegExp('(\\n' + spaces + 'd )(.+)((\\n| )*)(\\n' + spaces + 'd)', 'g');

		src = src.replace(removeSemicolonFromUnindented, function(){
			return arguments[1] + arguments[2].replace(/,? *$/m, '') + arguments[3] + arguments[5];//;
		});

		spaces = spaces+' ';
	}
	//console.log (src);
	return src;
};

module.exports = {
	compile: compile
};


